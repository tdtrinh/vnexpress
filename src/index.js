import React from 'react';
import ReactDOM from 'react-dom';

import HomePresentational from './js/components/presentational/Home.presentational';

require('./css/main.scss');

const wrapper = document.getElementById('app');
wrapper ? ReactDOM.render(<HomePresentational />, wrapper) : false;