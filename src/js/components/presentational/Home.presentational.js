import React, { Component } from 'react';
import { Button, CssBaseline, withStyles, FormControl, FilledInput, InputAdornment } from '@material-ui/core';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import PhoneInTalkIcon from '@material-ui/icons/PhoneInTalk';
import VideocamIcon from '@material-ui/icons/Videocam';
import MessageIcon from '@material-ui/icons/Message';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import classnames from 'classnames';

const styles = {
    button: {
        height: '20px',
        padding: '0 8px',
        color: 'white',
        minHeight: 'unset',
        minWidth: 'unset',
        textTransform: 'unset',
        fontSize: '12px',
        marginRight: '5px',
        marginTop: '-2px'
    },
    icon: {
        height: '16px',
        width: '16px',
        marginRight: '4px'
    },
    homeIcon: {
        marginBottom: '-3px',
        color: '#9f224e'
    },
    phoneIcon: {
        marginBottom: '-3px'
    },
    videoIcon: {
        marginBottom: '-3px',
        color: 'grey'
    },
    cameraIcon: {
        marginBottom: '-3px',
        color: 'grey'
    },
    messageIcon: {
        marginBottom: '-3px',
        marginRight: 'unset'
    },
    thumbUpButton: {
        backgroundColor: '#4267B2',
    },
    twitterButton: {
        backgroundColor: '#1B95E0'
    },
    rootInput: {
        marginTop: '5px',
        paddingRight: 'unset'
    },
    searchIcon: {
        marginBottom: '-3px',
        color: 'grey'
    },
    input: {
        fontSize: '12px',
        padding: 'unset !important',
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        '&:focus': {
            backgroundColor: 'white',
        }
    },
}

class HomePresentational extends Component {
    constructor() {
        super();
    }

    render() {
        let date = new Date();
        const { classes } = this.props;
        return (
            <React.Fragment>
                <CssBaseline />
                <div className="taskbar">
                    <div className="container">
                        <div className="inline">
                            <span>{date.toString()}</span>
                            <span className="divider" />
                            <a href="/">24h qua</a>
                            <span className="divider" />
                            <a href="/">RSS</a>
                            <span className="divider" />
                            <Button
                                variant="contained"
                                className={classnames(classes.button, classes.thumbUpButton)}
                            >
                                <ThumbUpIcon className={classes.icon} />
                                Like
                            </Button>
                            <Button
                                variant="contained"
                                className={classnames(classes.button, classes.twitterButton)}
                            >
                                <ThumbUpIcon className={classes.icon} />
                                Twitter
                            </Button>
                        </div>
                        <div className="inline right">
                            <FormControl>
                                <FilledInput
                                    placeholder="Tìm kiếm"
                                    classes={{
                                        root: classes.rootInput,
                                        input: classes.input
                                    }}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <SearchIcon className={classes.icon} />
                                        </InputAdornment>
                                    }
                                />
                            </FormControl>
                            <span className="divider" />
                            <a href="/">Đăng nhập</a>
                            <span className="divider" />
                            <a href="/">Đăng kí</a>
                        </div>
                        <div className="clearfix" />
                    </div>
                </div>
                <header className="logo">
                    <a href="/">
                        <img src="https://s.vnecdn.net/vnexpress/restruct/i/v63/graphics/img_logo_vne_web.gif" />
                    </a>
                </header>
                <nav className="nav">
                    <HomeIcon className={classnames(classes.icon, classes.homeIcon)} />
                    <a href="/">Video</a>
                    <a href="/">Thời sự</a>
                    <a href="/">Góc nhìn</a>
                    <a href="/">Thế giới</a>
                    <a href="/">Kinh doanh</a>
                    <a href="/">Giải trí</a>
                    <a href="/">Thể thao</a>
                    <a href="/">Pháp luật</a>
                    <a href="/">Giáo dục</a>
                    <a href="/">Sức khỏe</a>
                    <a href="/">Đời sống</a>
                    <a href="/">Du lịch</a>
                    <a href="/">Khoa học</a>
                    <a href="/">Số hóa</a>
                    <a href="/">Xe</a>
                    <a href="/">Cộng đồng</a>
                    <a href="/">Tâm sự</a>
                    <a href="/">Cười</a>
                </nav>
                <div className="breadcrumb">
                    <h4><a href="/">Trang nhất</a></h4>
                    <div className="arrow" />
                    <div className="right">
                        <span>
                            <PhoneInTalkIcon className={classnames(classes.icon, classes.phoneIcon)} />
                            <strong>083.888.0123</strong> (HN) - <strong>082.233.3555</strong> (TP HCM)
                        </span>
                    </div>
                    <div className="clearfix" />
                </div>
                <section className="featured">
                    <article>
                        <a href="/">
                            <img src="https://i-thethao.vnecdn.net/2018/11/20/anhtop-1542725491-4797-1542725517_500x300.jpg" />
                        </a>
                        <h1>
                            <a className="color-red" href="/">Bị tước bàn thắng hợp lệ, Việt Nam hòa Myanmar </a>
                            <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                            <a className="color-red" href="/">
                                <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                <span>440</span>
                            </a>
                        </h1>
                        <p>Các cầu thủ của HLV Park Hang-seo không tận dụng được những cơ hội ăn bàn, chấp nhận hoà 0-0 ở AFF Cup tối 20/11.</p>
                    </article>
                    <div className="sub-featured">
                        <a href="/">
                            <p>CĐV đốt pháo sáng mừng hụt pha sút bóng vào lưới gây tranh cãi của Văn Toàn</p>
                            <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                        </a>
                        <a href="/">
                            <p>Chó con chơi trượt máng trên thành cầu thang ở Trung Quốc</p>
                            <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                        </a>
                        <a href="/">
                            <p>Bệnh sốt rét tái xuất ở 13 quốc gia</p>
                        </a>
                        <a href="/">
                            <p>Trung Quốc và Philippines nâng cấp quan hệ, cam kết hợp tác ở Biển Đông</p>
                        </a>
                        <a href="/">
                            <p>Tiểu Vy giới thiệu bản thân bằng tiếng Anh tại Miss World</p>
                            <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                        </a>
                        <a href="/">
                            <p>Rùa biển bị thương được gắn chân giả sau 20 năm</p>
                            <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                        </a>
                        <a href="/">
                            <p>iPad Pro 2018 chính hãng có giá từ 21,99 triệu đồng</p>
                        </a>
                        <a href="/">
                            <p>Tàu hỏa trật bánh ở Tây Ban Nha, gần 50 người bị thương</p>
                        </a>
                        <a href="/">
                            <p>Bà Obama kiếm được 65 triệu USD nhờ viết sách</p>
                        </a>
                        <a href="/">
                            <p>Gài hai quả mìn ở nhà đối tác làm ăn</p>
                        </a>
                    </div>
                </section>
                <section className="main-container">
                    <section className="left-sidebar">
                        <article>
                            <h3>
                                <a href="/">
                                    <span>Bênh Công Phượng, thầy Park khẩu chiến với HLV Myanmar </span>
                                </a>
                                <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                                <PhotoCameraIcon className={classnames(classes.icon, classes.cameraIcon)} />
                                <a className="color-red" href="/">
                                    <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                    <p>21</p>
                                </a>
                            </h3>
                            <div className="art">
                                <img src="https://i-thethao.vnecdn.net/2018/11/20/park-1-1542726916_140x84.jpg" />
                            </div>
                            <div className="description">
                                <p>HLV Park Hang-seo cự cãi gay gắt với người đồng nghiệp Antoine Hey trong trận hòa 0-0 giữa Việt Nam và Myanmar ở AFF Cup tối 20/11.</p>
                                <ul>
                                    <li><a href="/">HLV Park Hang-seo: 'Hòa Myanmar vì thiếu may mắn'</a></li>
                                </ul>
                            </div>
                        </article>
                        <article>
                            <h3>
                                <a href="/">
                                    <span>Các tình huống tranh cãi của trọng tài trận Myanmar - Việt Nam </span>
                                </a>
                                <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                                <a className="color-red" href="/">
                                    <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                    <p>63</p>
                                </a>
                            </h3>
                            <div className="art">
                                <img src="https://i-vnexpress.vnecdn.net/2018/11/20/tai61341542720168-1542723454-5194-1542723741_140x84.jpg" />
                            </div>
                            <div className="description">
                                <p>Trọng tài người Qatar nhiều lần bỏ qua lỗi của các cầu thủ chủ nhà Myanmar, và từ chối pha làm bàn hợp lệ của Văn Toàn.</p>
                            </div>
                        </article>
                        <article>
                            <h3>
                                <a href="/">
                                    <span>HLV Park Hang-seo: 'Hòa Myanmar vì thiếu may mắn' </span>
                                </a>
                                <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                                <a className="color-red" href="/">
                                    <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                    <p>107</p>
                                </a>
                            </h3>
                            <div className="art">
                                <img src="https://i-thethao.vnecdn.net/2018/11/20/parkhangseo-1542721388-2318-1542721474_140x84.jpg" />
                            </div>
                            <div className="description">
                                <p>Nhà cầm quân của đội tuyển Việt Nam không hài lòng sau trận hòa 0-0 ở lượt trận thứ ba bảng A giải AFF Cup 2018 tối 20/11.</p>
                                <ul>
                                    <li><a href="/">Bị tước bàn thắng hợp lệ, Việt Nam hòa Myanmar</a></li>
                                </ul>
                            </div>
                        </article>
                        <article>
                            <h3>
                                <a href="/">
                                    <span>Ôtô VinFast có giá từ 336 triệu tại Việt Nam </span>
                                </a>
                                <a className="color-red" href="/">
                                    <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                    <p>609</p>
                                </a>
                            </h3>
                            <div className="art">
                                <img src="https://i-vnexpress.vnecdn.net/2018/11/20/img1358-1542703816-1542703833-8195-1542704598_140x84.jpg" />
                            </div>
                            <div className="description">
                                <p>Trong giai đoạn đầu bán hàng, xe cỡ nhỏ Fadil giá 336 triệu, sedan Lux A giá 800 triệu và Lux SA giá 1,166 tỷ.</p>
                            </div>
                        </article>
                        <article>
                            <h3>
                                <a href="/">
                                    <span>Cảnh sát vây bắt ôtô chở ma tuý trên đại lộ ở Sài Gòn </span>
                                </a>
                            </h3>
                            <div className="art">
                                <img src="https://i-vnexpress.vnecdn.net/2018/11/20/matuy-1542729266-6490-1542729270_140x84.jpg" />
                            </div>
                            <div className="description">
                                <p>Lái ôtô trên đại lộ Nguyễn Văn Linh thì bị CSGT "tuýt còi", Trường vừa xuống xe liền bị nhiều trinh sát mặc thường phục khống chế.</p>
                            </div>
                        </article>
                        <article>
                            <h3>
                                <a href="/">
                                    <span>HLV Myanmar hài lòng khi cầm hòa Việt Nam </span>
                                </a>
                                <VideocamIcon className={classnames(classes.icon, classes.videoIcon)} />
                                <a className="color-red" href="/">
                                    <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                    <p>84</p>
                                </a>
                            </h3>
                            <div className="art">
                                <img src="https://i-thethao.vnecdn.net/2018/11/20/myanmar-viet-nam-duc-dong91-15-3548-5697-1542724661_140x84.jpg" />
                            </div>
                            <div className="description">
                                <p>Ông Antoine Hey cho rằng một điểm giành được trước Việt Nam rất quan trọng, khi giúp Myanmar tiến gần hơn tới tấm vé dự bán kết AFF Cup 2018.</p>
                                <ul>
                                    <li><a href="/">HLV Park Hang-seo: 'Hòa Myanmar vì thiếu may mắn' / Bị tước bàn thắng hợp lệ, Việt Nam hòa Myanmar</a></li>
                                </ul>
                            </div>
                        </article>
                    </section>
                    <section className="center">
                        <article>
                            <hgroup>
                                <a href="/">Góc nhìn</a>
                            </hgroup>
                            <div className="content">
                                <a className="art" href="/">
                                    <img width="100%" src="https://i-vnexpress.vnecdn.net/2018/07/11/thanhnguyenpng-1531253299_100x100.png" />
                                    <p className="block">Thành Nguyễn</p>
                                </a>
                                <div className="description">
                                    <h4><a href="/">Chuyện xóm Núi</a></h4>
                                    <p>Các chỉ đạo "quyết liệt" là thứ có thể thực hiện hết lần này đến lần khác, cho tới tận khi có hậu quả ngoài mong muốn.</p>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <h5><a href="/">May mắn của học trò </a></h5>
                                    <a href="/" className="color-red">
                                        <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                        <p>51</p>
                                    </a>
                                    <p className="block marginBot8 marginTop4">Nguyễn Thị Thu Huệ</p>
                                </li>
                                <li>
                                    <h5><a href="/">Nghìn tỷ thẻ cào </a></h5>
                                    <a href="/" className="color-red">
                                        <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                        <p>74</p>
                                    </a>
                                    <p className="block marginBot8 marginTop4">Trương Thanh Đức</p>
                                </li>
                            </ul>
                        </article>
                        <article>
                            <hgroup>
                                <a href="/">Thể thao</a>
                            </hgroup>
                            <div className="content">
                                <a className="art" href="/">
                                    <img width="100%" src="https://i-thethao.vnecdn.net/2018/11/20/MYANMAR-VIET-NAM-DUC-DONG67-1542725302_140x84.jpg" />
                                </a>
                                <div className="description">
                                    <h4><a href="/">Cầu thủ Việt Nam ngã sấp mặt vì lối đá rắn của Myanmar</a></h4>
                                    <p>Myanmar chủ trương đá rắn, và vì thế, khiến các học trò của HLV Park Hang-seo nhiều lần nằm sân đau đớn trong trận hoà 0-0 hôm 20/11. ...</p>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <h5><a href="/">Những yếu tố giúp Myanmar cầm hòa Việt Nam   </a></h5>
                                    <a href="/" className="color-red">
                                        <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                        <p>51</p>
                                    </a>
                                </li>
                                <li>
                                    <h5><a href="/">Four Four Two: 'Trọng tài biên sai lầm khi từ chối bàn thắng cho Việt Nam'  </a></h5>
                                    <a href="/" className="color-red">
                                        <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                        <p>74</p>
                                    </a>
                                </li>
                            </ul>
                        </article>
                        <article>
                            <hgroup>
                                <a href="/">Thể thao</a>
                            </hgroup>
                            <div className="content">
                                <a className="art" href="/">
                                    <img width="100%" src="https://i-kinhdoanh.vnecdn.net/2018/11/21/VCG-1542741221-8477-1542764248_140x84.jpg" />
                                </a>
                                <div className="description">
                                    <h4><a href="/">Lý do nhà đầu tư tranh mua cổ phiếu Vinaconex</a></h4>
                                    <p>Lợi thế về quỹ đất hàng triệu m2 của công ty mẹ Vinaconex và những dự án lớn của các đơn vị trực thuộc giúp cổ phiếu VCG hấp dẫn. </p>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <h5><a href="/">Phiên giao dịch tệ nhất năm của tài chính toàn cầu </a></h5>
                                    <a href="/" className="color-red">
                                        <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                        <p>51</p>
                                    </a>
                                </li>
                                <li>
                                    <h5><a href="/">5 đại gia công nghệ mất 1.000 tỷ USD vốn hóa </a></h5>
                                    <a href="/" className="color-red">
                                        <MessageIcon className={classnames(classes.icon, classes.messageIcon)} />
                                        <p>74</p>
                                    </a>
                                </li>
                            </ul>
                        </article>
                    </section>
                </section>
                <footer>
                    <div className="top-footer">
                        <a href="/">Trang chủ</a>
                        <a href="/" className="right marginRight16">
                            <SearchIcon className={classnames(classes.icon, classes.searchIcon)} />
                            <span>Tìm kiếm</span>
                            <div className="divider" />
                            <ThumbUpIcon className={classnames(classes.icon, classes.searchIcon)} />
                            <div className="divider" />
                            <ThumbUpIcon className={classnames(classes.icon, classes.searchIcon)} />
                        </a>
                    </div>
                    <div className="menu">
                        <ul>
                            <li><a href="/">Video</a></li>
                            <li><a href="/">Thời sự</a></li>
                            <li><a href="/">Góc nhìn</a></li>
                            <li><a href="/">Thế giới</a></li>
                        </ul>
                        <ul>
                            <li><a href="/">Kinh doanh</a></li>
                            <li><a href="/">Giải trí</a></li>
                            <li><a href="/">Thể thao</a></li>
                            <li><a href="/">Pháp luật</a></li>
                        </ul>
                        <ul>
                            <li><a href="/">Giáo dục</a></li>
                            <li><a href="/">Sức khỏe</a></li>
                            <li><a href="/">Đời sống</a></li>
                            <li><a href="/">Du lịch</a></li>
                        </ul>
                        <ul>
                            <li><a href="/">Khoa học</a></li>
                            <li><a href="/">Số hóa</a></li>
                            <li><a href="/">Xe</a></li>
                            <li><a href="/">Cộng đồng</a></li>
                        </ul>
                        <ul>
                            <li><a href="/">Tâm sự</a></li>
                            <li><a href="/">Cười</a></li>
                            <li><a href="/">Ảnh</a></li>
                            <li><a href="/">Infographics</a></li>
                        </ul>
                        <ul>
                            <li><a href="/">Fsell</a></li>
                            <li><a href="/">Shop VnExpress</a></li>
                            <li><a href="/">Pay VnExpress</a></li>
                            <li><a href="/">Quà tặng</a></li>
                        </ul>
                        <div className="last-column">
                            <a href="/"><p>Tải ứng dụng</p></a>
                            <div className="store">
                                <img src="https://s.vnecdn.net/vnexpress/i/v46/graphics/apple_app.png" />
                                <img src="https://s.vnecdn.net/vnexpress/i/v46/graphics/android_app.png" />
                            </div>
                            <img src="https://s.vnecdn.net/vnexpress/restruct/i/v63/graphics/qr_app.jpg" />
                        </div>
                    </div>
                    <div className="bottom-footer">
                        <a href="/">
                            <img src="https://s.vnecdn.net/vnexpress/restruct/i/v63/graphics/img_logo_vne.jpg" />
                        </a>
                        <div className="copyright">
                            <p>© Copyright 1997 VnExpress. All rights reserved.</p>
                            <p>Thuộc Bộ Khoa học Công nghệ. Toàn bộ bản quyền thuộc VnExpress</p>
                        </div>
                    </div>
                </footer>
            </React.Fragment >
        );
    }
}

export default withStyles(styles)(HomePresentational);